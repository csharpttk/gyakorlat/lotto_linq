﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lotto_LINQ
{
    class Program
    {
        const int maxérték = 90;
        const int húzásszám = 5;
        static List<List<int>> húzások = new List<List<int>>();
        static int hetekszáma;
        static public void generálás()
        {
            int újhúzás, k;
            Random r = new Random();
            Console.Write("Hány hét? ");
            hetekszáma = Convert.ToInt16(Console.ReadLine()); //nincs ellenőrzés
            for (int i = 0; i < hetekszáma; i++)
            {
                húzások.Add(new List<int>());
                while (húzások[i].Count < húzásszám)
                {
                    újhúzás = r.Next(1, maxérték + 1);
                    if (!húzások[i].Contains(újhúzás))
                    {
                        húzások[i].Add(újhúzás);
                    }
                }
                húzások[i].Sort();
                Console.WriteLine(String.Join(" ", húzások[i]));
            }
        }
        static public void statisztika()
        {
            var eredmény = húzások.SelectMany(x => x)
                                  .GroupBy(x=>x)
                                  .Select(y=>new { szám = y.Key, gyakoriság = y.Count() })
                                  .OrderBy(r=>r.szám);
            foreach (var sz in eredmény)
            {
                Console.WriteLine("{0} - {1} ", sz.szám, sz.gyakoriság);
            }
        }
        static void Main(string[] args)
        {
            generálás();
            statisztika();
        }
    }

}
